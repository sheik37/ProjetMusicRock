from .app import db,login_manager
from flask_login import UserMixin


class Author(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return "<Author (%d) %s>" % (self.id, self.name)
    def get_id(self):
        return self.id
    def get_name(self):
        return self.name
    def set_name(self,name):
        self.name=name

class Genre(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    genre = db.Column(db.String(100))

    def __repr__(self):
        return "<Genre (%d) %s>" % (self.id, self.genre)
    def get_id(self):
        return self.id
    def get_genre(self):
        return self.genre
    def set_genre(self,genre):
        self.genre=genre

class Parent(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    compositeur = db.Column(db.String(100))

    def __repr__(self):
        return "<Compositeur (%d) %s>" % (self.id, self.compositeur)
    def get_id(self):
        return self.id
    def get_compositeur(self):
        return self.compositeur
    def set_compositeur(self,comp):
        self.compositeur=comp

class Album(db.Model):
    id  =   db.Column(db.Integer,primary_key=True)
    img =   db.Column(db.String(100))
    releaseYear =   db.Column(db.Integer)
    title   =   db.Column(db.String(100))
    author_id   =   db.Column(db.Integer,db.ForeignKey("author.id"))
    author  = db.relationship("Author",backref=db.backref("albums",lazy="dynamic"))
    parent_id  =   db.Column(db.Integer,db.ForeignKey("parent.id"))
    parent  = db.relationship("Parent",backref=db.backref("parents",lazy="dynamic"))
    genre_id   =   db.Column(db.String(100),db.ForeignKey("genre.id"))
    genre  = db.relationship("Genre",backref=db.backref("genres",lazy="dynamic"))

    def __repr__(self):
        return "<Album (%d) %s>" % (self.id, self.title)
    def get_id(self):
        return self.id
    def get_img(self):
        return self.img
    def set_img(self,img):
        self.img=img
    def get_author_id(self):
        return self.author_id
    def set_author_id(self,author_id):
        self.author_id=author_id
    def get_title(self):
        return self.title
    def set_title(self,title):
        self.title=title
    def get_genre(self):
        return self.genre_id
    def set_genre(self,genre):
        self.genre_id=genre
    def get_releaseyear(self):
        return self.releaseYear
    def set_releaseyear(self,releaseyear):
        self.releaseYear=releaseyear
    def get_parent(self):
        return self.parent_id
    def set_parent(self,parent):
        self.parent_id=parent

class User(db.Model, UserMixin):
    username = db.Column(db.String(50), primary_key=True)
    password = db.Column(db.String(64))

    def get_id(self):
        return self.username
    def set_username(self,username):
        self.username=username
    def set_password(self,password):
        self.password=password
    def __repr__(self):
        return "<User (%s)>" %(self.username)

class Playlist(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(100))
    album_id = db.Column(db.String(100),db.ForeignKey("album.id"))
    album = db.relationship("Album",backref=db.backref("albums",lazy="dynamic"))
    user_id = db.Column(db.String(100),db.ForeignKey("user.username"))
    user = db.relationship("User",backref=db.backref("users",lazy="dynamic"))

    def __repr__(self):
        return "<Playlist (%d) %s>" % (self.id, self.name)

    def get_id(self):
        return self.id
    def get_name(self):
        return self.name
    def set_name(self,name):
        self.name=name
    def get_album_id(self):
        return self.album_id
    def set_album_id(self,album_id):
        self.album_id=album_id
    def get_username_id(self):
        return self.user_id
    def set_username_id(self,username_id):
        self.user_id=username_id
#
# fonction pour le user
#
def get_user(username):
    return User.query.filter(User.username==username).one()

def get_user_all():
    return User.query.all()

@login_manager.user_loader
def load_user(username):
    return User.query.get(username)

#
# fonction pour les auteurs
#
def get_author(id):
    return Author.query.get(id)

def get_author_name(id):
    return Author.query.get(id).name

def get_author_all():
    return Author.query.all()

def search_author(rech):
    return Author.query.filter(Author.name.like('%'+rech+'%')).all()

#
# fonction pour les albums
#
def get_album(id):
    return Album.query.get(id)

def get_album_author(id):
    return Album.query.filter(Album.author_id==id).all()

def get_album_all():
    return Album.query.all()

def search_album(rech):
    return Album.query.filter(Album.title.like('%'+rech+'%')).all()

#
# fonction pour les genres
#

def get_genre(id):
    return Genre.query.get(id)

def get_genre_all():
    return Genre.query.all()

def affiche_genre(chaine):
    chaine=chaine.split(',')
    chaine[0]=chaine[0][1:]
    chaine[-1]=chaine[-1][:-1]
    genre=[]
    for elem in chaine:
        genre.append(elem)
    return genre

#
# fonction pour les compositeurs
#
def get_compositeur(id):
    return Parent.query.get(id)

def get_compositeur_all():
    return Parent.query.all()

#
# fonction pour les playlist
#

def get_playlist(id):
    return Playlist.query.get(id)

def get_playlist_all():
    return Playlist.query.all()

def affiche_album(chaine):
    chaine=chaine.split(',')
    chaine[0]=chaine[0][1:]
    chaine[-1]=chaine[-1][:-1]
    album=[]
    for elem in chaine:
        album.append(int(elem))
    return album
