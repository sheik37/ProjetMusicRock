from .app import app, db
from flask import render_template, url_for, redirect, request
from .models import *
from flask_wtf import FlaskForm
from wtforms import StringField,HiddenField,PasswordField,IntegerField
from wtforms.validators import DataRequired
from hashlib import sha256
from flask_login import login_user,current_user, logout_user, login_required

#
# Les formulairess
#

class AuthorForm(FlaskForm):
	id = HiddenField('id')
	name =StringField('Nom', validators=[DataRequired()])

	def get_id(self):
		return self.id.data
	def get_name(self):
		return self.name.data

class AlbumForm(FlaskForm):
	id = HiddenField('id')
	name = StringField('Nom', validators=[DataRequired()])
	date = IntegerField('Date', validators=[DataRequired()])

	def get_id(self):
		return self.id.data
	def get_name(self):
		return self.name.data
	def get_date(self):
		return self.date.data

class PlaylistForm(FlaskForm):
	id = HiddenField('id')
	name =StringField('Nom', validators=[DataRequired()])

	def get_id(self):
		return self.id.data
	def get_name(self):
		return self.name.data

class LoginForm(FlaskForm):
	username = StringField('Username')
	password = PasswordField('Password')
	next = HiddenField()

	def get_authenticated_user(self):
		user = User.query.get(self.username.data)
		if user is None:
			return None
		m = sha256()
		m.update(self.password.data.encode())
		passwd = m.hexdigest()
		return user if passwd == user.password else None
	def get_id(self):
		return self.username.data
	def get_password(self):
		return self.password.data
	def get_next(self):
		return self.next.data
	def set_next(self, newNext):
		self.next.data = newNext

class InscriptionForm(FlaskForm):
	username = StringField("Username")
	password = PasswordField("Password")
	passwords = PasswordField("Confirmer password")
	def get_id(self):
		return self.username.data
	def get_mdp(self):
		return self.password.data
	def uniq_Username(self):
		return load_user(self.username.data) == None
	def passwd_confirmed(self):
		return self.password.data == self.passwords.data

#
# Les fonction de login
#

@app.route("/login/",methods=("GET","POST",))
def login():
	f= LoginForm()
	if not f.is_submitted():
		f.set_next(request.args.get("next"))
	elif f.validate_on_submit():
		user = f.get_authenticated_user()
		if user:
			login_user(user)
			next = f.get_next() or url_for("home")
			return redirect(next)
	return render_template("login.html",form=f)

@app.route("/inscription/",methods=("GET","POST",))
def inscription():
	f=InscriptionForm()
	text=""
	return render_template("inscription.html",form=f,p=text)

@app.route("/inscription/save/", methods=["POST"])
def save_inscription():
	user = None
	f = InscriptionForm()
	if f.validate_on_submit():
		if f.uniq_Username():
			if f.passwd_confirmed():
				m = sha256()
				m.update(f.get_mdp().encode())
				if f.validate_on_submit():
					user = User(
						username = f.get_id(),
						password = m.hexdigest())
					db.session.add(user)
					db.session.commit()
					return redirect(url_for('login'))
			else:
				text="Les mots de passe ne sont pas identiques"
		else:
			text="Vous possédez déjà un compte"
	return render_template(
		"inscription.html",
		form=f,p=text)

@app.route("/logout/")
def logout():
    logout_user()
    return redirect(url_for('home'))

#
# Les fonctions pour la page d'accueil
#

@app.route("/")
def home():
	return render_template(
		"home.html",
		title="La musique en illimité!",)

@app.route("/Home")
def home_page():
	return render_template(
	"home-page.html"
	)

#
# Les fonctions des auteurs
#

@app.route("/Author/")
def author():
	return render_template(
	"author.html",
	liste_authors = get_author_all())

@app.route("/Author/<int:id>")
def one_author(id):
	a=get_author(id)
	return render_template(
	"one_author.html",
	author=a,
	album = get_album_author(id))

@app.route("/Author/edit/<int:id>")
@login_required
def edit_author(id):
	a = get_author(id)
	return render_template(
		"edit-author.html",
		author= a,
		form=AuthorForm(id=a.get_id(), name=a.get_name()))

@app.route("/Author/save/", methods=["POST"])
def save_author():
	a= None
	f=AuthorForm()
	if f.validate_on_submit():
		if f.id.data == "":
			auteur=f.get_name()[0].upper()+f.get_name()[1:]
			a=Author(name=auteur)
			db.session.add(a)
		else:
			id=int(f.get_id())
			a=get_author(id)
			a.set_name(f.get_name())
		db.session.commit()
		return redirect(url_for('one_author',id=a.get_id()))
	a=get_author(int(f.get_id()))
	return render_template(
		"edit-author.html",
		album=a, form=f)

@app.route("/Author/new/")
@login_required
def new_author():
	f=AuthorForm(id=None,name="")
	return render_template(
		"new-author.html",
		form=f)

@app.route("/Author/delete/",methods=["POST","GET"])
@login_required
def delete_author():
	p=""
	if request.method=="POST":
		a=get_author(int(request.form['del']))
		db.session.delete(a)
		db.session.commit()
		p="L'artiste à était supprimé"
	la=get_author_all()
	liste=[]
	for auteur in la:
		liste.append((auteur.name,auteur.id))
	liste.sort()
	return render_template("delete-author.html",liste=liste,text=p)

#
# Les fonction des albums
#

@app.route('/Album/index/')
@app.route('/Album/index/<int:page>',methods=['GET'])
def album(page=1):
	per_page = 6
	posts=Album.query.paginate(page,per_page,error_out=False)
	return render_template('album.html',posts=posts,page=page,maxPage=posts.pages)

@app.route("/Album/<int:id>")
def info_album(id):
	a=get_album(id)
	genre=""
	for g in affiche_genre(a.get_genre()):
		genre+=get_genre(g).get_genre()+", "
	genre=genre[0:len(genre)-2]
	return render_template(
	"one_album.html",
	album=a,
	auteur=get_author(a.get_author_id()),
	compositeur=get_compositeur(a.get_parent()),
	genre=genre)

@app.route("/Album/edit/<int:id>")
@login_required
def edit_album(id):
	comp=get_compositeur_all()
	genre=get_genre_all()
	auteurs=get_author_all()
	a = get_album(id)
	auteur=get_author(a.get_author_id())
	g=affiche_genre(a.get_genre())
	genre_album=[get_genre(100000)]*3
	for i in range(len(g)):
		genre_album[i]=get_genre(g[i])
	return render_template(
		"edit-album.html",
		album=a,
		auteur_album=auteur,
		genre_album=genre_album,
		compositeur_al=get_compositeur(a.get_parent()),
		form=AlbumForm(id=a.get_id(), name=a.get_title(),date=a.get_releaseyear()),
		compositeur=comp,
		genre=genre,
		auteur=auteurs)

@app.route("/Album/save/", methods=["POST"])
def save_album():
	a= None
	f=AlbumForm()
	if f.validate_on_submit():
		if f.get_id() == "":
			if request.method=="POST":
				album=f.get_name()[0].upper()+f.get_name()[1:]
				genre=[int(request.form["Genre1"]),int(request.form["Genre2"]),int(request.form["Genre3"])]
				liste=[]
				for i in range(3):
					if genre[i]==100000:
						liste.append(i)
				liste.reverse()
				for elem in liste:
					del genre[elem]
				if genre==[]:
					genre=[100000]
				a=Album(title=album,img="default.png",releaseYear=f.get_date(),author_id=int(request.form['Auteur']),parent_id=request.form['Compositeur'],genre_id=str(genre))
				db.session.add(a)
		else:
			id=int(f.get_id())
			a=get_album(id)
			a.set_title(f.get_name())
			a.set_releaseyear(f.get_date())
			a.set_parent(request.form['Compositeur'])
			a.set_author_id(request.form['Auteur'])
			genre=[int(request.form["Genre1"]),int(request.form["Genre2"]),int(request.form["Genre3"])]
			liste=[]
			for i in range(3):
				if genre[i]==100000:
					liste.append(i)
			liste.reverse()
			for elem in liste:
				del genre[elem]
			if genre==[]:
				genre=[100000]
			a.set_genre(str(genre))
		db.session.commit()
		return redirect(url_for('info_album',id=a.get_id()))
	a=get_album(int(f.get_id()))
	return render_template(
		"edit-album.html",
		album=a, form=f)

@app.route("/Album/new/")
@login_required
def new_album():
	album=get_album_all()
	comp=get_compositeur_all()
	genre=get_genre_all()
	la=get_author_all()
	f=AlbumForm(id=None,name="",date="")
	p=""
	return render_template(
		"new-album.html",
		form=f,
		liste=la,
		comp=comp,
		genre=genre)

@app.route("/Album/delete/",methods=["POST","GET"])
@login_required
def delete_album():
	p=""
	if request.method=="POST":
		a=get_album(int(request.form['del']))
		db.session.delete(a)
		db.session.commit()
		p="L'album à était supprimé"
	la=get_album_all()
	liste=[]
	for album in la:
		liste.append((album.get_title(),album.get_id()))
	liste.sort()
	return render_template("delete-album.html",liste=liste,text=p)

#
# Les fonction des playlist
#

@app.route('/Playlist/index/')
@app.route('/Playlist/index/<int:page>',methods=['GET'])
def playlist(page=1):
	per_page = 6
	posts=Playlist.query.paginate(page,per_page,error_out=False)
	return render_template('playlist.html',posts=posts,page=page,maxPage=posts.pages)

@app.route("/Playlist/<int:id>")
def info_playlist(id):
	play=get_playlist(id)
	liste_album=affiche_album(play.get_album_id())
	albums=[]
	print(albums)
	for album in liste_album:
		if album!=0:
			albums.append(get_album(album))
	return render_template(
	"one_playlist.html",
	playlist=play,
	album = albums)

@app.route("/Playlist/edit/<int:id>")
@login_required
def edit_playlist(id):
	play = get_playlist(id)
	return render_template(
		"edit-playlist.html",
		playlist= play,
		form=AuthorForm(id=play.get_id(), name=play.get_name()))

@app.route("/Playlist/save/", methods=["POST"])
def save_playlist():
	play= None
	f=PlaylistForm()
	if f.validate_on_submit():
		if f.get_id() == "":
			playliste=f.get_name()[0].upper()+f.get_name()[1:]
			play=Playlist(name=playliste,user_id=current_user.username,album_id=str([0,0]))
			db.session.add(play)
		else:
			id=int(f.get_id())
			play=get_playlist(id)
			play.set_name(f.get_name())
		db.session.commit()
		return redirect(url_for('info_playlist',id=play.get_id()))
	play=get_playlist(int(f.get_id()))
	return render_template(
		"edit-playlist.html",
		playlist=play, form=f)

@app.route("/Playlist/new/")
@login_required
def new_playlist():
	f=PlaylistForm(id=None,name="")
	return render_template(
		"new-playlist.html",
		form=f)

@app.route("/Playlist/delete/",methods=["POST","GET"])
@login_required
def delete_playlist():
	p=""
	if request.method=="POST":
		a=get_playlist(int(request.form['del']))
		db.session.delete(a)
		db.session.commit()
		p="La playlist à était supprimé"
	play=get_playlist_all()
	liste=[]
	for playlist in play:
		if current_user.username==playlist.get_username_id():
			liste.append((playlist.get_name(),playlist.get_id()))
	liste.sort()
	return render_template("delete-playlist.html",liste=liste,text=p)

@app.route("/Playlist/ajouter/<int:id>", methods=["POST","GET"])
@login_required
def ajouter_playlist(id):
		p=""
		if request.method=="POST":
			playlist=get_playlist(int(request.form['ajout']))
			album=affiche_album(playlist.get_album_id())
			liste=[]
			for i in range(len(album)):
				if album[i]==0:
					liste.append(i)
			liste.reverse()
			for elem in liste:
				del album[elem]
			album.append(id)
			if len(album)==1:
				album.append(0)
			print(album)
			playlist.set_album_id(str(album))
			db.session.commit()
			p="L'album a était ajouté à la playlist"
		play=get_playlist_all()
		liste=[]
		for playlist in play:
			if current_user.username==playlist.get_username_id():
				liste.append((playlist.get_name(),playlist.get_id()))
		liste.sort()
		return render_template("ajouter-playlist.html",liste=liste,text=p,album=get_album(id))

@app.route("/Playlist/enlever/<int:id>", methods=["POST","GET"])
@login_required
def enlever_playlist(id):
		p=""
		if request.method=="POST":
			playlist=get_playlist(id)
			album=affiche_album(playlist.get_album_id())
			for elem in album:
				if elem==int(request.form['enlever']):
					album.remove(elem)
			if album==[]:
				album.append(0)
				album.append(0)
			playlist.set_album_id(str(album))
			db.session.commit()
			p="L'album a était enlevé de la playlist"
		playlist=get_playlist(id)
		album=affiche_album(playlist.get_album_id())
		liste=[]
		for elem in album:
			alb=get_album(elem)
			if alb!=None:
				liste.append((alb.get_title(),alb.get_id()))
		liste.sort()
		return render_template("enlever-playlist.html",liste=liste,text=p,playlist=get_playlist(id))

#
# autre fonctionnalité
#

@app.route("/contacts")
def contact():
	return render_template('contact.html')

@app.route("/search/", methods=["POST"])
def search():
	rech=request.form['recherche']
	liste_authors_s = search_author(rech)
	liste_album_s = search_album(rech)
	return render_template("search.html", liste_a = liste_authors_s, liste_al = liste_album_s,recherche=rech)
