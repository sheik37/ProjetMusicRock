from flask import Flask
app = Flask(__name__)
app.debug = True

from flask_script import Manager
manager = Manager(app)

app.config['BOOTSTRAP_SERVE_LOCAL'] = True
from flask_bootstrap import Bootstrap
Bootstrap(app)

import os.path
def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),
            p))

from flask_sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI']=('sqlite:///'+mkpath("../tuto.db"))
db = SQLAlchemy(app)

app.config['SECRET_KEY'] = "f80e3c9d-4229-4e14-a302-7b624a52f6eb"

from flask_login import LoginManager
login_manager = LoginManager(app)
login_manager.login_view = "login"
