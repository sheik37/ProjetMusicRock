from .app import manager,db

@manager.command
def loaddb(filename):
    db.create_all()

    import yaml

    liste_albums = yaml.load(open(filename,encoding='utf_8'))

    from .models import Author,Parent,Genre,Album,Playlist

    authors = {}
    for a in liste_albums:
        author = a["by"][0].upper()+a["by"][1:]
        if author not in authors:
            if author=="X":
                o = Author(name="Inconnu")
            else:
                o = Author(name=author)
            db.session.add(o)
            authors[author]=o
    db.session.commit()
    db.session.add(Playlist(name="voiry",album_id=str([10707,10770]),user_id="voiry"))
    db.session.commit()
    genres={}
    for g in liste_albums:
        genre=g["genre"]
        for elem in genre:
            elem=elem[0].upper()+elem[1:]
            if elem not in genres:
                o=Genre(genre=elem)
                db.session.add(o)
                genres[elem]=o
    db.session.add(Genre(id=100000,genre="Aucun"))
    db.session.commit()

    compositeurs={}
    for c in liste_albums:
        compositeur = c["parent"][0].upper()+c["parent"][1:]
        if compositeur not in compositeurs:
            o= Parent(compositeur=compositeur)
            db.session.add(o)
            compositeurs[compositeur]=o
    db.session.add(Parent(compositeur="Inconnu"))
    db.session.commit()

    for a in liste_albums:
        b=authors[a["by"]]
        c=compositeurs[a["parent"]]
        g=[]
        if a["img"]==None:
            img="default.png"
        else:
            img=a["img"]
        if a["genre"]!=[]:
            for genre in a["genre"]:
                genre=genre[0].upper()+genre[1:]
                gs=genres[genre]
                g.append(gs.id)
        else:
            g.append(100000)
        album = Album(id= a["entryId"],
                    genre_id=str(g),
                    img=img,
                    parent_id=c.id,
                    releaseYear=a["releaseYear"],
                    title=a["title"][0].upper()+a["title"][1:],
                    author_id=b.id)
        db.session.add(album)
        db.session.commit()

@manager.command
def syncdb():
    '''Creates all missing tables.'''
    db.create_all()

@manager.command
def newuser(username,password):
    from .models import User
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = User(username=username,password=m.hexdigest())
    db.session.add(u)
    db.session.commit()

@manager.command
def passwd(username,password):
    from .models import User,get_user
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    user=get_user(username)
    user.set_password(m.hexdigest())
    db.session.commit()
